package it.uniroma3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgettosiwApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgettosiwApplication.class, args);
	}
}
